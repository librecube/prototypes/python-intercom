import threading
import pickle
from multiprocessing.connection import Listener, Client

from collections import namedtuple
Command = namedtuple('Command', ('name', 'function', 'respond'))


class IntercomServer:

    def __init__(self, host, port):
        self._address = (host, port)
        self._listener = Listener(self._address)
        self._thread = threading.Thread(target=self._loop)
        self._commands = []

    def _loop(self):
        thread = threading.currentThread()
        while not thread.kill:
            with self._listener.accept() as conn:
                msg = conn.recv()
                if not msg:
                    continue
                command_name, *args = pickle.loads(msg)
                for command in self._commands:
                    if command_name == command.name:
                        result = command.function(*args)
                        if command.respond:
                            conn.send(pickle.dumps(result))

    def start(self):
        self._thread.kill = False
        self._thread.start()

    def stop(self):
        self._thread.kill = True
        with Client(self._address) as conn:
            conn.send("")  # do a dummy connection to interrupt server loop
        self._thread.join()
        self._listener.close()

    def register_command(self, command, function, respond=False):
        self._commands.append(Command(command, function, respond))


class IntercomClient:

    def __init__(self, host, port):
        self._address = (host, port)

    def send(self, command, *args, respond=False):
        with Client(self._address) as conn:
            conn.send(pickle.dumps([command, *args]))
            if respond:
                return pickle.loads(conn.recv())
