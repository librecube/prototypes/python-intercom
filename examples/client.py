from intercom import IntercomClient
import click

client = IntercomClient('localhost', 7389)


@click.group()
def cli():
    pass


@cli.command()
@click.argument('name')
def hello(name):
    client.send('hello', name)


@cli.command()
@click.argument('message')
def echo(message):
    print(client.send('echo', message, respond=True))


cli()
