from intercom import IntercomServer


def hello(name):
    print("hello from", name)


def echo(message):
    print(f"echo '{message}'")
    return message


server = IntercomServer('localhost', 7389)
server.register_command('hello', hello, respond=False)
server.register_command('echo', echo, respond=True)


server.start()
input("Enter to stop")
server.stop()
